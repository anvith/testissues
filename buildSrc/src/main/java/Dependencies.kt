object Versions {

    //app
    val compileSdk = 28
    val minSdk = 21
    val targetSdk = 28

    // Android libraries
    val appCompatVersion = "1.0.2"
    val materialVersion = "1.0.0"
    val vectorVersion = "1.0.1"
    val browserVersion = "1.0.0"
    val annotationsVersion = "1.0.0"
    val multidexVersion = "2.0.0"

    // Android Architecture
    val lifecycleVersion = "2.0.0"

    val leakCanaryVersion = "1.6.1"

    // java networking libraries
    val retrofitVersion = "2.4.0"
    val okhttpVersion = "3.11.0"
    val chuckVersion = "1.1.0"

    // Rx
    val rxJavaVersion = "2.2.0"
    val rxAndroidVersion = "2.0.2"
    val rxPermissionVersion = "0.10.2"

    // dagger
    val daggerVersion = "2.17"
    val jsr250Version = "1.0"

    //test libraries
    val junitVersion = "4.12"
    val tesetRunnerVersion = "1.1.0"
    val hamcrestVersion = "1.4-atlassian-1"
    val espressoVersion = "3.1.0"
    val mockitoVersion = "2.8.9"
    val mockitoKotlinVersion = "1.6.0"

    //Glide
    val glideVersion = "4.2.0"
    val glideTransformVersion = "3.0.1"
    val gpuImageVersion = "1.4.1"

    //logging
    val timberVersion = "4.6.0"

    // Code Style
    val ktLintVersion = "0.11.1"

    // Kotlin
    val kotlinVersion = "1.2.51"
}

object Deps {
    val appCompat = "androidx.appcompat:appcompat:${Versions.appCompatVersion}"
    val materialComponents = "com.google.android.material:material:${Versions.materialVersion}"
    val vectorSupport = "androidx.vectordrawable:vectordrawable:${Versions.vectorVersion}"
    val chromeTabs = "androidx.browser:browser:${Versions.browserVersion}"
    val multidexSupport = "androidx.multidex:multidex:${Versions.multidexVersion}"

    val leakCanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leakCanaryVersion}"
    val leakCanaryNoOp = "com.squareup.leakcanary:leakcanary-android-no-op:${Versions.leakCanaryVersion}"

    val chuck = "com.readystatesoftware.chuck:library:${Versions.chuckVersion}"
    val chuckNoOp = "com.readystatesoftware.chuck:library-no-op:${Versions.chuckVersion}"

    val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycleVersion}"
    val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.lifecycleVersion}"

    val dagger = "com.google.dagger:dagger:${Versions.daggerVersion}"
    val daggerCompiler = "com.google.dagger:dagger-compiler:${Versions.daggerVersion}"
    val jsr250 = "javax.annotation:jsr250-api:${Versions.jsr250Version}"

    val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJavaVersion}"
    val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroidVersion}"
    val rxPermission = "com.github.tbruyelle:rxpermissions:${Versions.rxPermissionVersion}"

    val glide = "com.github.bumptech.glide:glide:${Versions.glideVersion}"
    val glideOkHttp = "com.github.bumptech.glide:okhttp3-integration:${Versions.glideVersion}"
    val glideTransform = "jp.wasabeef:glide-transformations:${Versions.glideTransformVersion}"
    val gpuImage = "jp.co.cyberagent.android.gpuimage:gpuimage-library:${Versions.gpuImageVersion}"

    val okHttp = "com.squareup.okhttp3:okhttp:${Versions.okhttpVersion}"
    val okHttpLogging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttpVersion}"

    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}"
    val retrofitGsonConvertor = "com.squareup.retrofit2:converter-gson:${Versions.retrofitVersion}"
    val retrofitRxAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofitVersion}"
    val timber = "com.jakewharton.timber:timber:${Versions.timberVersion}"
    val mockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.okhttpVersion}"

    val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}"
}

object TestDeps {
    val junit = "junit:junit:${Versions.junitVersion}"
    val supportAnnotations = "androidx.annotation:annotation:${Versions.annotationsVersion}"
    val testRunner = "androidx.test:runner:${Versions.tesetRunnerVersion}"
    val testRule = "androidx.test:rules:${Versions.tesetRunnerVersion}"
    val hamcrest = "org.hamcrest:hamcrest-library:${Versions.hamcrestVersion}"
    val espresso = "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    val mockito = "org.mockito:mockito-core:${Versions.mockitoVersion}"
    val mockitoKotlin = "com.nhaarman:mockito-kotlin:${Versions.mockitoKotlinVersion}"
}