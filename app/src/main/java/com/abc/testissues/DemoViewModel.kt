
package com.abc.testissues

import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

class DemoViewModel (val schedulersProvider: SchedulersProvider, val handler:DemoContract.Handler) : DemoContract.ViewModel() {

    var viewEventsStream: PublishSubject<DemoContract.ViewEvent>? = null
        set(value) {
            field = value
            subscribeToViewEvents()
        }

    private fun subscribeToViewEvents() {
        viewEventsStream?.let {
            it.subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.mainThread())
                .subscribe(object:Observer<DemoContract.ViewEvent>{
                    override fun onComplete() {
                        println("Completed") //Temporary println for  debugging unit tests not meant to be used as is
                    }

                    override fun onSubscribe(d: Disposable) {
                        println("Subscribed $d")
                    }

                    override fun onNext(t: DemoContract.ViewEvent) {
                        onViewEvent(t)
                    }

                    override fun onError(e: Throwable) {
                        println("Error: $e")
                    }

                })
        }
    }
     fun onViewEvent(event: DemoContract.ViewEvent) {
        when (event) {
            is DemoContract.ViewEvent.UpdateClick -> {
               handler.onUpdate()
            }
        }
    }

}
