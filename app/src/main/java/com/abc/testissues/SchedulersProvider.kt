package com.abc.testissues

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executor

open class SchedulersProvider  {

    open fun mainThread(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    open fun io(): Scheduler {
        return Schedulers.io()
    }

    open fun computation(): Scheduler {
        return Schedulers.computation()
    }

    open fun newThread(): Scheduler {
        return Schedulers.newThread()
    }

    open fun trampoline(): Scheduler {
        return Schedulers.trampoline()
    }

    open fun single(): Scheduler {
        return Schedulers.single()
    }

    fun from(executor: Executor): Scheduler {
        return Schedulers.from(executor)
    }
}