package com.abc.testissues

interface DemoContract {
    abstract class ViewModel

    sealed class ViewEvent {
        object UpdateClick : ViewEvent()
    }

    interface Handler{
        fun onUpdate()
    }
}